#!/usr/bin/env python
import json
import os
import argparse
parser = argparse.ArgumentParser(description="Generate a list of tasks (dummy program)")
parser.add_argument("--nbtasks", help="Number of tasks to simulate", type=int, default=10)
params = parser.parse_args()

tasks=[f"hello from task {i}" for i in range(params.nbtasks)]
item_file = os.environ["ITEMS_FILE"]
with open(item_file, 'w') as f:
  json.dump(tasks, f)
with open(os.environ["COUNT_FILE"], 'w') as f:
    f.write(str(len(tasks)))
