FROM python:3.10-slim
COPY script.py /app/script.py
RUN chmod +x /app/script.py
ARG USER=bee
ENV HOME /home/$USER
RUN adduser $USER
USER $USER
WORKDIR $HOME
